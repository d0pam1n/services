module github.com/project-flogo/services/flow-state

require (
	github.com/julienschmidt/httprouter v1.3.0
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.2.0 // indirect
	go.uber.org/zap v1.9.1
)

go 1.13
